/**
 * 
 */
package test;

import static org.junit.Assert.*;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.PrintStream;
import java.util.ArrayList;

import main.Driver;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.lsmr.vendingmachine.simulator.Coin;

/**
 * Test all pop selection functionality
 * @author Andrei Savu, Arthur Lee, Jade White, Jan Clarin, Olabode Adegbayike
 *
 */
public class PopSelectionTest {

	InputStream stin = System.in;
	void setUpInputStream(String input)
	{
		ByteArrayInputStream in = new ByteArrayInputStream(input.getBytes());
		System.setIn(in);
	}
	void tearDownInputStream() 
	{
		System.setIn(stin);
	}
	
	PrintStream stdout = System.out;
	ByteArrayOutputStream setUpOutputStream()
	{
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		PrintStream ps = new PrintStream(baos);
		System.setOut(ps);
		return baos;
	}
	void tearDownOutputStream()
	{
		System.setOut(stdout);
	}
	
	//---Output String Helpers
	String trimAboveNextBreak(String theString)
	{//Trim all above the next break.
		int breakStart = theString.indexOf("-----");
		int breakEnd = theString.indexOf('\n', breakStart+1);
		return theString.substring(breakEnd+1, theString.length()-1);
	}
	
	String trimBelowNextBreak(String theString)
	{//Trim all below the next break
		int breakStart = theString.indexOf("-----");
		return theString.substring(0, breakStart);
	}
	
	private String convertPriceToString(int value) {
		float amount = ((float) value) / 100;
		return String.format("$%.2f", amount);
	}
	
	Driver driver;
	int[] CoinValues = { 5, 10, 25, 100, 200 };
	int[] PopCosts = { 50, 100, 225, 250, 300, 125 };
	String[] PopNames = { "Coke", "Pepsi", "7UP", "Mountain Dew", "Sprite", "Fanta" }; 
	
	@Before
	public void setUp() throws Exception 
	{	
		driver = new Driver(CoinValues, PopNames, PopCosts);
		driver.fillPopRacks(10);
		driver.fillCoinRacks(10);
	}

	@After
	public void tearDown() throws Exception 
	{
		driver = null;
	}
	
	@Test
	//Are the prices of each pop item set correctly?
	public void testCorrectPopPrice()
	{
		String input = "3 \n 1 \n " /*Buy a coke*/ + "4\n" /*Quit*/;
		setUpInputStream(input);
		ByteArrayOutputStream ob = setUpOutputStream();
		
		driver.beginSimulation();
		
		String output = ob.toString();
		
		tearDownOutputStream();
		tearDownInputStream();
		
		//--Trim down our output to get the section with the pop listings.
		output = trimAboveNextBreak(output);
		output = trimAboveNextBreak(output);
		
		output = trimBelowNextBreak(output);
				
		//--Find the pop prices & names
		ArrayList<String> parsedPrices = new ArrayList<String>();
		ArrayList<String> parsedNames = new ArrayList<String>();
		
		//Isolate prices and names
		int startIndex = 0;
		while (true) 
		{
			int dollarIndex = output.indexOf('$',startIndex);
			if (dollarIndex == -1)
				break;
			
			int dashIndex = output.indexOf('-',dollarIndex+1);
			parsedPrices.add(output.substring(dollarIndex, dashIndex).trim());
						
			int newLineIndex = output.indexOf('\n',dashIndex+1);
			parsedNames.add(output.substring(dashIndex+1, newLineIndex).trim());
			
			startIndex = newLineIndex+1;
		}
		
		//--Check that all popnames and prices match up correctly.
		for (int i = 0; i < PopNames.length; i++) 
		{
			String currPopName = PopNames[i];
			String currPopPrice = convertPriceToString(PopCosts[i]);
			
			int parsedNameIndex = parsedNames.indexOf(currPopName);
			
			assertTrue("PopName exists?",(parsedNameIndex != -1));
			if (parsedNameIndex == -1) {return;}
			
			String parsedName = parsedNames.get(parsedNameIndex);
			String parsedPrice = parsedPrices.get(parsedNameIndex);
			
			assertTrue(parsedName.equals(currPopName));
			assertTrue(parsedPrice.equals(currPopPrice));
		}
	}
	
	@Test
	//Check that there is one button for each type of pop
	public void testCorrectPopRepresentation()
	{
		String input = "3 \n 1 \n " /*Buy a coke*/ + "4\n" /*Quit*/;
		setUpInputStream(input);
		ByteArrayOutputStream ob = setUpOutputStream();
		
		driver.beginSimulation();
		
		String output = ob.toString();
		
		tearDownOutputStream();
		tearDownInputStream();
		
		//--Trim down our output to get the section with the pop listings.
		output = trimAboveNextBreak(output);
		output = trimAboveNextBreak(output);
		
		output = trimBelowNextBreak(output);
				
		//--Find the pop count
		int buttonCount = 0;
		int startIndex = 0;
		while ((startIndex = output.indexOf('$',startIndex)) != -1)
		{
			startIndex++;
			buttonCount++;
		}
		
		assertTrue(buttonCount == PopNames.length);
	}
	
	@Test
	//Check that a pop is dispensed if you have exactly the right amount of funds.
	public void testPopVendingExactFunds()
	{
		String input = 	"1 \n 25 \n 1 \n 25 \n " + /*Insert 50 cents*/ 
						"3 \n 1  \n " /*Buy a coke*/ + 
						"4 \n" /*Quit*/;
		setUpInputStream(input);
		ByteArrayOutputStream ob = setUpOutputStream();
		
		driver.beginSimulation();
		
		String output = ob.toString();
		
		tearDownOutputStream();
		tearDownInputStream();
		
		assertTrue(output.indexOf("Coke was vended") != -1);
	}
	
	@Test
	//Check that pop is dispensed if you have extra coins. Also check that extra coins are returned.
	public void testPopVendingExcessFunds()
	{
		String input = 	"1 \n 200 \n " + /*Insert 200 cents*/ 
						"3 \n 1  \n " /*Buy a coke*/ + 
						"4 \n" /*Quit*/;
		setUpInputStream(input);
		ByteArrayOutputStream ob = setUpOutputStream();

		driver.beginSimulation();

		String output = ob.toString();

		tearDownOutputStream();
		tearDownInputStream();

		assertTrue(output.indexOf("Coke was vended") != -1); //We got our coke
		
		Coin[] returnedCoins = driver.getCoinsFromChute();
		int totalReturnedCoinValue = 0;
		for (int i = 0; i < returnedCoins.length; i++) 
			totalReturnedCoinValue += returnedCoins[i].getValue();
		
		assertTrue(totalReturnedCoinValue == 150); //We got 150 cents in change
	}
	
	@Test
	//Check that pop is NOT dispensed if you don't have enough coins.
	public void testPopVendingLowFunds()
	{
		String input = 	"1 \n 25 \n" + /*Insert 25 cents*/ 
						"3 \n 1  \n " /*Buy a coke*/ + 
						"4 \n" /*Quit*/;
		setUpInputStream(input);
		ByteArrayOutputStream ob = setUpOutputStream();

		driver.beginSimulation();

		String output = ob.toString();

		tearDownOutputStream();
		tearDownInputStream();
		
		assertFalse(output.indexOf("Coke was vended") != -1); //We didn't get our coke
	}

	@Test
	public void testPopVendingNoStock()
	{
		int initialPopQuantity = 10;
		int timesTryingToBuyOverQuantity = 5;
		
		String input = "";
		for (int i = 0; i < (initialPopQuantity+timesTryingToBuyOverQuantity); i++) 
			input += "1 \n 100 \n  3 \n 2 \n ";  //BUY ALL THE POP! +1 too many.
		input += " 4 \n";
		
		setUpInputStream(input);
		ByteArrayOutputStream ob = setUpOutputStream();

		driver.beginSimulation();

		String output = ob.toString();
		tearDownOutputStream();
		tearDownInputStream();
						
		//--Find the pop count
		int vendedCount = 0;
		int startIndex = 0;
		while ((startIndex = output.indexOf("Pepsi was vended",startIndex)) != -1)
		{
			startIndex++;
			vendedCount++;
		}
				
		assertEquals(initialPopQuantity,vendedCount);
	}
	
	@Test
	public void testPopVendingCoinsFull()
	{
		int maxReceptableCapacity = 40;
		String input = "";
		for (int i = 0; i < maxReceptableCapacity; i++) 
			input += " 1 \n 100 \n "; /*Add coins*/
		input += "3 \n 1\n " /*Vend the pop*/ + " 4 \n";
		
		setUpInputStream(input);
		ByteArrayOutputStream ob = setUpOutputStream();
		
		driver.beginSimulation();

		String output = ob.toString();
		tearDownOutputStream();
		tearDownInputStream();
		
		Coin[] returnedCoins = driver.getCoinsFromChute();
		int totalReturnedCoinValue = 0;
		for (int i = 0; i < returnedCoins.length; i++) 
			totalReturnedCoinValue += returnedCoins[i].getValue();
		
		assertEquals((100*maxReceptableCapacity - 50),totalReturnedCoinValue); //We got 150 cents in change
		assertTrue(output.indexOf("Coke was vended") != -1); //We got our coke
	}
}
