/**
 * 
 */
package test;

import static org.junit.Assert.*;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.PrintStream;

import main.Driver;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * Test Class to check exact Change functionality in Driver class
 * @author Andrei Savu, Arthur Lee, Jade White, Jan Clarin, Olabode Adegbayike
 *
 */
public class ChangeTest {

	InputStream stin = System.in;
	void setUpInputStream(String input)
	{
		ByteArrayInputStream in = new ByteArrayInputStream(input.getBytes());
		System.setIn(in);
	}
	void tearDownInputStream() 
	{
		System.setIn(stin);
	}
	
	PrintStream stdout = System.out;
	ByteArrayOutputStream setUpOutputStream()
	{
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		PrintStream ps = new PrintStream(baos);
		System.setOut(ps);
		return baos;
	}
	void tearDownOutputStream()
	{
		System.setOut(stdout);
	}
	
	//---Output String Helpers
	String trimAboveNextBreak(String theString)
	{//Trim all above the next break.
		int breakStart = theString.indexOf("-----");
		int breakEnd = theString.indexOf('\n', breakStart+1);
		return theString.substring(breakEnd+1, theString.length()-1);
	}
	
	String trimBelowNextBreak(String theString)
	{//Trim all below the next break
		int breakStart = theString.indexOf("-----");
		return theString.substring(0, breakStart);
	}
	
	private String convertPriceToString(int value) {
		float amount = ((float) value) / 100;
		return String.format("$%.2f", amount);
	}
	
	String getReturnedMoneyString(int startIndex, String output)
	{
		//Returned Change is : $0.50
		int wordsStartIndex = output.indexOf("Returned Change",startIndex);
		if (wordsStartIndex == -1){return null;}
		
		int moneyStartIndex = 1 + output.indexOf(':', wordsStartIndex+1);
		int endOfLineIndex = output.indexOf('\n', moneyStartIndex);
		
		return output.substring(moneyStartIndex, endOfLineIndex).trim();
	}
	
	Driver driver; //Instantiate Driver class
	@Before
	public void setUp() throws Exception {
		int[] CoinValues = { 5, 10, 25, 100, 200 };
		int[] PopCosts = { 50, 100, 225, 250, 300, 125 };
		String[] PopNames = { "Coke", "Pepsi", "7UP", "Mountain Dew", "Sprite", "Fanta" }; 
		
		driver = new Driver(CoinValues, PopNames, PopCosts);
		driver.fillPopRacks(5); //Initialize # of pop in pop rack
		driver.fillCoinRacks(10); // Initialize # of coins in each coin rack
	}

	/**
	 * Reset Driver to null after each test
	 */
	@After
	public void tearDown() throws Exception {
		driver = null;
	}

	@Test
	//Try buying a coke (50 cents) using a dollar. Do we get 50 cents back?
	public void testCorrectChange00()
	{
		int expectedReturnedCoins = 50;
		
		String input = "1 \n 100 \n " /*Insert 1$*/ + " 3 \n 1 \n " /*Buy a coke*/ + "4\n" /*Quit*/;
		setUpInputStream(input);
		ByteArrayOutputStream ob = setUpOutputStream();
		
		driver.beginSimulation();
		
		String output = ob.toString();
		
		tearDownOutputStream();
		tearDownInputStream();
		
		String moneyReturnedString = getReturnedMoneyString(0,output);
		assertTrue(moneyReturnedString != null);
		System.out.println(moneyReturnedString);
		
		assertTrue(moneyReturnedString.equals(convertPriceToString(expectedReturnedCoins)));
	}
	
	@Test
	//Try buying a pepsi (100 cents) using a two dollars. Do we get 100 cents back?
	public void testCorrectChange01()
	{
		int expectedReturnedCoins = 100;
		
		String input = "1 \n 200 \n " /*Insert 1$*/ + " 3 \n 2 \n " /*Buy a coke*/ + "4\n" /*Quit*/;
		setUpInputStream(input);
		ByteArrayOutputStream ob = setUpOutputStream();
		
		driver.beginSimulation();
		
		String output = ob.toString();
		
		tearDownOutputStream();
		tearDownInputStream();
		
		String moneyReturnedString = getReturnedMoneyString(0,output);
		assertTrue(moneyReturnedString != null);
		System.out.println(moneyReturnedString);
		
		assertTrue(moneyReturnedString.equals(convertPriceToString(expectedReturnedCoins)));
	}

}
