/**
 * 
 */
package test;

import static org.junit.Assert.assertTrue;
import java.io.ByteArrayInputStream;

import main.Driver;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.lsmr.vendingmachine.simulator.CapacityExceededException;

/**
 * Unit test class for Driver class methods.
 * @author  Andrei Savu, Arthur Lee, Jade White, Jan Clarin, Olabode Adegbayike
 *
 */
public class DriverTest {

	/**
	 * Initialize Driver class
	 */
	
	Driver driver;
	
	@Before
	public void setUp() throws Exception {
		
		int[] CoinValues = { 5, 10, 25, 100, 200 }; // Acceptable coin values. $0.05 $0.10 $0.25 $1 $2
		int[] PopCosts = { 50, 100, 225, 250, 300, 125 }; //Pop Cost matches Pop
		String[] PopNames = { "Coke", "Pepsi", "7UP",
					"Mountain Dew", "Sprite", "Fanta" }; 
		driver = new Driver(CoinValues, PopNames, PopCosts);
	}

	/**
	 * Reset driver after every test
	 */
	@After
	public void tearDown() throws Exception {
		driver = null;
	}
	
	void setUpScanner(String input){
		ByteArrayInputStream in = new ByteArrayInputStream(input.getBytes());
		System.setIn(in);
	}
	

	/**
	 * Try to fill coin rack with more than capacity allows
	 * @throws CapacityExceededException
	 */
	
	@Test
	public void testFillCoinrackExceeded() throws CapacityExceededException {
		driver.fillCoinRacks(21);
	}

	/**
	 * Try to fill pop rack with more than capacity allows
	 * @throws CapacityExceededException
	 */
	
	@Test
	public void testFillPoprackExceeded() throws CapacityExceededException {
		driver.fillPopRacks(21);
	}
	
	/**
	 * Successfully fill in coins to the coin rack
	 */
	
	@Test
	public void testFillCoinrack(){
		driver.fillCoinRacks(5);
		driver.fillCoinRacks(10);
		
	}
	
	/**
	 * Successfully fill in coins to the coin rack
	 */
	
	@Test
	public void testFillPoprack(){
		driver.fillCoinRacks(5);
		driver.fillCoinRacks(10);
	}
	
	/**
	 * Test Pop is in right spots
	 * [0]Coke, [1]Pepsi, [2]7UP
	 */
	@Test
	public void testCoinsFromChute(){
		String input = "1 \n 10\n 4";
		setUpScanner(input);
		
		driver.beginSimulation();
		
		String pk = driver.getProductKind(2).getName();
		String pk2 = driver.getProductKind(1).getName();
		String pk3  = driver.getProductKind(0).getName();
		
		assertTrue("7UP".equals(pk));
		assertTrue("Pepsi".equals(pk2));
		assertTrue("Coke".equals(pk3));
		
	}
}
