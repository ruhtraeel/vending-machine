/**
 * 
 */
package test;

import static org.junit.Assert.*;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.PrintStream;

import main.Driver;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.lsmr.vendingmachine.simulator.Coin;

/**
 * Tests all coin selsction and return functionality
 * @author Andrei Savu, Arthur Lee, Jade White, Jan Clarin, Olabode Adegbayike
 *
 */
public class CoinEntryTest {

	Driver driver;
	InputStream stin = System.in;
	PrintStream stdout = System.out;
	
	void setUpInputStream(String input)
	{
		ByteArrayInputStream in = new ByteArrayInputStream(input.getBytes());
		System.setIn(in);
	}
	void tearDownInputStream() 
	{
		System.setIn(stin);
	}
	
	ByteArrayOutputStream setUpOutputStream()
	{
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		PrintStream ps = new PrintStream(baos);
		System.setOut(ps);
		return baos;
	}
	
	void tearDownOutputStream()
	{
		System.setOut(stdout);
	}
	
	private String convertPriceToString(int value) {
		float amount = ((float) value) / 100;
		return String.format("$%.2f", amount);
	}
	
	String getLastReturnedMoneyString(int startIndex, String output)
	{
		//Returned Change is : $0.50
		int wordsStartIndex = output.indexOf("Coins returned to user",startIndex);
		if (wordsStartIndex == -1){return null;}
		
		int moneyStartIndex = 1 + output.indexOf(':', wordsStartIndex+1);
		int endOfLineIndex = output.indexOf('\n', moneyStartIndex);
		
		return output.substring(moneyStartIndex, endOfLineIndex).trim();
	}
	
	
	/**
	 * Set up of:
	 * Acceptable Coin values
	 * Cost of Each pop
	 * Pop Names
	 */
	@Before
	public void setUp() throws Exception {
		
		// Valid coin denominations.
		int[] CoinValues = { 5, 10, 25, 100, 200 };
		
		// Pop costs.
		int[] PopCosts = { 50, 100, 225, 250, 300, 125 };
		// Pop names.
		String[] PopNames = { "Coke", "Pepsi", "7UP",
					"Mountain Dew", "Sprite", "Fanta" }; 
		
		driver = new Driver(CoinValues, PopNames, PopCosts);
		driver.fillPopRacks(9);
		driver.fillCoinRacks(19);
	}
	
	
	@After
	public void tearDown() throws Exception {
		driver = null;
		System.setIn(System.in);
	}
	
	void setUpScanner(String input){
		ByteArrayInputStream in = new ByteArrayInputStream(input.getBytes());
		System.setIn(in);
	}

	/**
	 * Test coin denomination is accepted when valid
	 * @throws 
	 */
	@Test
	public void testCoinValidity() {
		String input = "1 \n 10\n 4";
		setUpScanner(input);
		driver.beginSimulation();
		assertTrue(driver.getCoinsFromChute().length == 0); 
		
	}
	
	/**
	 * Test coin denomination is rejected when invalid
	 */
	@Test
	public void testCoinValidity2() {
		String input = "1 \n 1000 \n 4";
		setUpScanner(input);
		driver.beginSimulation();
		Coin[] coinsReturned = driver.getCoinsFromChute();
		assertEquals(1, coinsReturned.length); 
		assertEquals(1000, coinsReturned[0].getValue());
	}
	
	/**
	 * Test storage of valid coins
	 */
	
	@Test
	public void testCoinTracking() {
		String input = "1 \n 10 \n 1 \n 25 \n 1 \n 100 \n 4";
		setUpScanner(input);
		driver.beginSimulation();
		assertEquals(135, driver.getCoinsAvailable().getTotalCoinsAvailable());
	}
	
	
	/**
	 * Check to see if coin returns when receptacle is full
	 */
	
	@Test
	public void testCoinReceptacleFull(){
		String input = "";
		for (int i = 0; i < 52; i++) {
			input+= "1 \n 10 \n ";
		}
		input+= "4";
		System.out.println(input);
		setUpScanner(input);
		driver.beginSimulation();
		Coin[] coinsReturned = driver.getCoinsFromChute();
		
		assertEquals(2, coinsReturned.length); 
		
		int total = 0;
		for (int i = 0; i < coinsReturned.length; i++) {
			total += coinsReturned[i].getValue();
		}
		assertEquals(20, total);
	}
	
	/**
	 * Test for return button pressed
	 * All coins in receptacle should be returned
	 */
	@Test
	public void testReturnButtonValidity(){
		int expectedReturnedCoins = 235;
		
		String input = "1 \n 10 \n 1 \n 25 \n 1 \n 200 \n 2 \n 4";
		setUpInputStream(input);
		ByteArrayOutputStream ob = setUpOutputStream();
		
		driver.beginSimulation();
		
		String output = ob.toString();
		
		tearDownOutputStream();
		tearDownInputStream();
		/*Coin[] coinsReturned = driver.getCoinsFromChute();
		assertEquals(3, coinsReturned.length); 
		
		int total = 0;
		for (int i = 0; i < coinsReturned.length; i++) {
			total += coinsReturned[i].getValue();
		}
		assertEquals(235, total);*/
		
		String moneyReturnedString = getLastReturnedMoneyString(0,output);
		assertTrue(moneyReturnedString != null);
		System.out.println(moneyReturnedString);
		
		assertTrue(moneyReturnedString.equals(convertPriceToString(expectedReturnedCoins)));
		
	}
	
	@Test
	public void testCoinRackcapacity(){
		
		int expectedReturnedCoins = 25;

		
		String input = "1 \n 100 \n 3 \n 2 \n 1 \n 25 \n 4";
		setUpInputStream(input);
		ByteArrayOutputStream ob = setUpOutputStream();
		
		driver.beginSimulation();
		
		String output = ob.toString();
		
		tearDownOutputStream();
		tearDownInputStream();
		
		String moneyReturnedString = getLastReturnedMoneyString(0,output);
		assertTrue(moneyReturnedString != null);
		System.out.println(moneyReturnedString);
		
		assertTrue(moneyReturnedString.equals(convertPriceToString(expectedReturnedCoins)));
		
		/*Coin[] coinsReturned = driver.getCoinsFromChute();
		
		assertEquals(1, coinsReturned.length); 
		
		int total = 0;
		for (int i = 0; i < coinsReturned.length; i++) {
			total += coinsReturned[i].getValue();
		}
		assertEquals(25, total);*/
		
		
	}

}
