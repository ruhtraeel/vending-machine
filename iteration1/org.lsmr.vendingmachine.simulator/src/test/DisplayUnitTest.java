package test;

import static org.junit.Assert.assertTrue;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import main.Driver;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * Test all the system displays
 * @author Andrei Savu, Arthur Lee, Jade White, Jan Clarin, Olabode Adegbayike
 *
 */

public class DisplayUnitTest {
	
	Driver driver;
	ByteArrayOutputStream baos;

	/**Set up of:
		 * Acceptable Coin values
		 * Cost of Each pop
		 * Pop Names
	**/
	
	@Before
	public void setUp() throws Exception {
		int[] CoinValues = { 5, 10, 25, 100, 200 };
		int[] PopCosts = { 50, 100, 225, 250, 300, 125 };
		String[] PopNames = { "Coke", "Pepsi", "7UP", "Mountain Dew", "Sprite", "Fanta" }; 
		
		driver = new Driver(CoinValues, PopNames, PopCosts);
		driver.fillPopRacks(1);
		driver.fillCoinRacks(19);
	}
	
	/**
	 * Tear down driver and reset system.in and system.out
	 * @throws Exception
	 */

	@After
	public void tearDown() throws Exception {
		driver = null;
		System.setIn(System.in);
		System.setOut(System.out);
	}
	
	private void setUpScanner(String input) {
		ByteArrayInputStream in = new ByteArrayInputStream(input.getBytes());
		System.setIn(in);	
	}
	
	private void setUpOutputStream(){
		baos = OutputStream();
		PrintStream ps = new PrintStream(baos);
		System.setOut(ps);
	}
	
	private ByteArrayOutputStream OutputStream(){
		baos = new ByteArrayOutputStream();
		return baos;
	}


	/**
	 * Test Display for coin entry
	 */
	@Test
	public void testCoinDisplayValidity() {
		String input = "1 \n 10 \n 1 \n 25 \n 1 \n 200 \n 4";
		setUpScanner(input);
		
		setUpOutputStream();
		
		driver.beginSimulation();
		
		String output = baos.toString();
		int begIndex = output.lastIndexOf('$');
		int endIndex = output.indexOf('\n', begIndex);
		String substr = output.substring(begIndex+1, endIndex);
		System.setOut(System.out);
		System.out.println();
		assertTrue("2.35".equals(substr));
	}
	
	/**
	 * Test Out of Order sign and wait time for Coin rack full
	 */
	@Test (timeout = 4100)
	public void testRackFullDisplay(){
		String input = "1 \n 100 \n 3 \n 2 \n 1 \n 25 \n 4";
		setUpScanner(input);
		
		setUpOutputStream();
		
		driver.beginSimulation();
		
		String output = baos.toString();
		int begIndex = output.lastIndexOf("Out of Order:");
		int endIndex = output.indexOf('\n', begIndex);
		String substr = output.substring(begIndex+14, endIndex);
		System.setOut(System.out);
		assertTrue("On".equals(substr));
		
	}

	/**
	 * Test wait time for pop out of stock
	 */
	@Test (timeout = 5030)
	public void testPopOutOfStockDisplay(){
		String input = "1 \n 100 \n 3 \n 2 \n 1 \n 100 \n 3 \n 2 \n 4";
		setUpScanner(input);
		
		driver.beginSimulation();
	}
	
	/**
	 * Test display for not enough funds
	 */
	@Test(timeout = 4020)
	public void testNotEnoughFundsDisplay(){
		String input = "1 \n 25 \n 3 \n 4 \n 4";
		setUpScanner(input);
		
		driver.beginSimulation();
	}
		
}
