package main;

/**
 * Test driver class for the vending machine with sample inputs.
 * 
 * @author Andrei Savu, Arthur Lee, Jade White, Jan Clarin, Olabode Adegbayike
 */
public class TestDriver {
	private static final int[] SAMPLE_COINS = new int[] { 5, 10, 25, 100, 200 };
	private static final String[] SAMPLE_POP_NAMES = new String[] { "Coke",
			"Pepsi", "7UP", "Mountain Dew", "Sprite", "Fanta", "Dr. Pepper",
			"Barg's Root Beer", "San Pellegrino", "Crush" };
	private static final int[] SAMPLE_POP_COSTS = new int[] { 50, 100, 25, 250,
			300, 110, 120, 150, 100, 200 };

	private final static int INIT_POP_QTY = 10;
	private final static int INIT_COIN_QTY = 10;

	public static void main(String[] args) {
		Driver driver = new Driver(SAMPLE_COINS, SAMPLE_POP_NAMES,
				SAMPLE_POP_COSTS);
		driver.fillPopRacks(INIT_POP_QTY);
		driver.fillCoinRacks(INIT_COIN_QTY);
		driver.beginSimulation();
	}
}
