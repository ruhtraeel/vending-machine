package main;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Locale;
import java.util.Scanner;

import org.lsmr.vendingmachine.funds.CoinsAvailable;
import org.lsmr.vendingmachine.funds.Currency;
import org.lsmr.vendingmachine.product.ProductKind;
import org.lsmr.vendingmachine.simulator.CapacityExceededException;
import org.lsmr.vendingmachine.simulator.Coin;
import org.lsmr.vendingmachine.simulator.CoinRackSimulator;
import org.lsmr.vendingmachine.simulator.DisabledException;
import org.lsmr.vendingmachine.simulator.HardwareSimulator;
import org.lsmr.vendingmachine.simulator.PopCan;

/**
 * Driver class for the vending machine.
 * 
 * @author Andrei Savu, Arthur Lee, Jade White, Jan Clarin, Olabode Adegbayike
 *
 */
public class Driver {
	// Main options.
	private static final String[] OPTIONS = new String[] { "Insert Coin",
			"Return Coins", "Select Pop", "Quit" };

	// Valid coin denominations.
	private int[] coinValues;

	// Pop names.
	private String[] popNames;

	// Pop costs.
	private int[] popCosts;

	// Hardware simulator.
	private HardwareSimulator simulator;

	// Maintains the coins available listens to hardware notifications.
	private CoinsAvailable coinsAvailable;

	// Maintains the pop logic for each kind.
	private ProductKind[] popKinds;

	/**
	 * Public constructor that begins the simulation.
	 * 
	 * @param coinValues
	 * @param popNames
	 * @param popCosts
	 */
	public Driver(int[] coinValues, String[] popNames, int[] popCosts) {
		this.coinValues = coinValues;
		this.popNames = popNames;
		this.popCosts = popCosts;
		simulator = new HardwareSimulator(coinValues, popCosts, popNames);
		setUp();
	}

	/**
	 * Begins the text-based UI simulation.
	 */
	public void beginSimulation() {
		// Input scanner.
		Scanner scanner = new Scanner(System.in);

		// Checks if coin rack.
		if (isAnyCoinRackFull()) {
			toggleOutOfOrderLight(true);
		}

		// Checks if there isn't change available.
		if (!isChangeAvailable()) {
			toggleExactChangeLight(true);
		}

		// Print the display to console.
		printDisplay("$0.00\n");

		while (true) {
			// Main menu selection.
			int selection = promptMainSelection(scanner);

			// Handle user selection.
			switch (selection) {
			case 0: // Insert coin.
				// Prompt user for coin value.
				int coinValue = promptCoinValue(scanner);
				Coin coin = new Coin(coinValue);
				// Insert coin once a valid coin value has been chosen.
				insertCoin(coin);
				break;
			case 1: // Return coins from receptacle.
				returnReceptacleCoins();
				break;
			case 2: // Provide options for pop selection buttons.
				int selectedPopIndex = promptPopSelection(scanner);
				selectButton(selectedPopIndex);
				break;
			case 3: // Quit simulation.
				scanner.close();
				return;
			}
		}
	}

	/**
	 * Returns all of the coins currently in the delivery chute.
	 * 
	 * @return
	 */
	public Coin[] getCoinsFromChute() {
		ArrayList<Coin> coins = new ArrayList<Coin>();

		Object[] objects = simulator.getDeliveryChute().removeItems();

		for (Object object : objects) {
			if (object instanceof Coin) {
				coins.add((Coin) object);
			}
		}

		return coins.toArray(new Coin[coins.size()]);
	}

	/**
	 * Returns the coinsAvailable instance for testing coins.
	 * 
	 * @return
	 */
	public CoinsAvailable getCoinsAvailable() {
		return coinsAvailable;
	}

	/**
	 * Returns a ProductKind for pop index.
	 * 
	 * @param index
	 * @return
	 */
	public ProductKind getProductKind(int index) {
		return popKinds[index];
	}

	/**
	 * Sets up the CoinsAvailable and ProductKinds.
	 */
	private void setUp() {
		// Get list of racks for CoinsAvailable instantiation.
		CoinRackSimulator[] racks = new CoinRackSimulator[coinValues.length];
		for (int i = 0; i < coinValues.length; i++) {
			racks[i] = simulator.getCoinRack(i);
		}

		// Maintains the state of coins in the machine.
		coinsAvailable = new CoinsAvailable(simulator.getCoinReceptacle(),
				coinValues, racks);

		// Currency type.
		java.util.Currency currencyType = java.util.Currency
				.getInstance(Locale.CANADA);

		// Initialize pop kinds.
		popKinds = new ProductKind[popNames.length];

		// Initialize ProductKind for each pop.
		for (int i = 0; i < popNames.length; i++) {
			Currency popCost = new Currency(currencyType, new BigDecimal(
					popCosts[i]));
			popKinds[i] = new ProductKind(popNames[i], popCost,
					simulator.getPopCanRack(i), simulator.getSelectionButton(i));
		}
	}

	/**
	 * Returns a boolean indicating if there is a full coin rack.
	 * 
	 * @return true if there exists a full coin rack.
	 */
	private boolean isAnyCoinRackFull() {
		for (int i = 0; i < coinValues.length; i++) {
			if (!simulator.getCoinRack(i).hasSpace()) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Returns false if any of the racks have less than 4 coins.
	 * 
	 * @param popId
	 * @return
	 */
	private boolean isChangeAvailable() {
		for (int i = 0; i < coinValues.length; i++) {
			if (coinsAvailable.getRackQuantity(i) < 4) {
				return false;
			}
		}
		return true;
	}

	/**
	 * Fill all pop racks with given quantity.
	 * 
	 * @param quantity
	 */
	public void fillPopRacks(int quantity) {
		// Add INIT_POP_QTY num of pops to each pop rack
		for (int i = 0; i < popNames.length; i++) {
			for (int j = 0; j < quantity; j++) {
				try {
					simulator.getPopCanRack(i).addPop(new PopCan());
				} catch (CapacityExceededException e) {
					System.err.println("ERROR: Pop racks capacity exceeded");
				} catch (DisabledException e) {
					System.err.println("ERROR: Pop racks disabled");
				}
			}
		}
	}

	/**
	 * Fill all coin racks with given quantity.
	 * 
	 * @param quantity
	 */
	public void fillCoinRacks(int quantity) {
		// Add coins to coin rack
		for (int i = 0; i < coinValues.length; i++) {
			for (int j = 0; j < quantity; j++) {
				try {
					simulator.getCoinRack(i)
							.acceptCoin(new Coin(coinValues[i]));
				} catch (CapacityExceededException e) {
					System.err.println("ERROR: Coin racks capacity exceeded");
				} catch (DisabledException e) {
					System.err.println("ERROR: Coin racks are disabled");
				}
			}
		}
	}

	/**
	 * Prompts user for main selection.
	 * 
	 * @param scanner
	 * @return
	 */
	private int promptMainSelection(Scanner scanner) {
		int selection = -1;

		// Prompt user for valid option.
		while (selection < 0 || selection >= OPTIONS.length) {
			System.out.println("Choose an option:");
			for (int i = 0; i < OPTIONS.length; i++) {
				System.out.printf("[%d] %s ", i + 1, OPTIONS[i]);
			}
			System.out.println();

			// Main option selection.
			selection = scanner.nextInt() - 1;
		}

		return selection;
	}

	/**
	 * Prompts user for coin value and returns the coin value.
	 * 
	 * @param scanner
	 * @return
	 */
	private int promptCoinValue(Scanner scanner) {
		System.out.print("Coin value (in cents): ");
		int coinValue = scanner.nextInt();
		System.out.println();
		return coinValue;
	}

	/**
	 * Prompts user for pop selection index and returns selected pop button
	 * index.
	 * 
	 * @param scanner
	 * @return
	 */
	private int promptPopSelection(Scanner scanner) {
		// Pop button index.
		int popButtonIndex = -1;

		// Prompt user until user inputs a valid button selection.
		while (popButtonIndex < 0 || popButtonIndex >= popNames.length) {
			// Provide pop button options starting from 1.
			for (int i = 0; i < popNames.length; i++) {
				System.out.printf("[%d] %s - %s\n", i + 1,
						getValueString(popCosts[i]), popNames[i]);
			}
			System.out.println();

			System.out.print("Choose a pop button: ");

			// Subtract 1 as the selection values starts at 1.
			popButtonIndex = scanner.nextInt() - 1;

			System.out.println();
		}

		return popButtonIndex;
	}

	/**
	 * Insert coin into coin slot.
	 * 
	 * @param coin
	 * @return boolean indicating success.
	 */
	private boolean insertCoin(Coin coin) {
		// Put coin into coin slot.
		try {
			simulator.getCoinSlot().addCoin(coin);
		} catch (DisabledException e) {
			return false;
		}

		if (isAnyCoinRackFull()) {
			// Return the coin.
			returnReceptacleCoins();
			String coinString = getValueString(coin.getValue());
			System.out.println("Returned coin is : " + coinString);

			// Display out of order light.
			toggleOutOfOrderLight(true);
			displayUpdatedCoinValue();
			return false;
		} else {
			if (simulator.getCoinReceptacle().hasSpace()) {
				displayUpdatedCoinValue();
				return true;
			} else {
				// Coin receptacle is full.
				return false;
			}
		}
	}

	/**
	 * Returns all coins in the coin receptacle.
	 * 
	 * @return boolean indicating success.
	 */
	private void returnReceptacleCoins() {
		coinsAvailable.returnCoins(coinsAvailable.getCoinsAvailable());
		Coin[] temp = getCoinsFromChute();

		int total = 0;
		for (Coin coin : temp) {
			total += coin.getValue();
		}
		System.out.println("Coins returned to user: " + getValueString(total));
	}

	/**
	 * Presses the selection button within the hardware.
	 * 
	 * @param popId
	 */
	private void selectButton(int popId) {
		simulator.getSelectionButton(popId).press();
		
		// Check if there if the selected pop is out of stock.
		if (popKinds[popId].getQuantityAvailable() < 1) {
			displayOutOfStock();
			displayUpdatedCoinValue();
			return;
		}

		int totalValueCoins = coinsAvailable.getTotalCoinsAvailable();

		// If exact change just dispense.
		if (totalValueCoins == popCosts[popId]) {
			if (isChangeAvailable()) {
				// Dispense pop.
				popKinds[popId].dispense();

				// Store coins.
				coinsAvailable.storeCoins(coinsAvailable.getCoinsAvailable());

				// Display vended pop
				System.out.println(popNames[popId] + " was vended");
			} else {
				// If there is not enough change, return coins and turn on light
				toggleExactChangeLight(true);
				returnReceptacleCoins();

				// Print out to console.
				String coinString = getValueString(totalValueCoins);
				System.out.println("Returned coins: " + coinString);
			}

			// Update Display
			displayUpdatedCoinValue();
		} else if (totalValueCoins > popCosts[popId]) {
			if (isChangeAvailable()) {
				// Dispense pop.
				popKinds[popId].dispense();

				// Calculate the change
				Currency change = new Currency(
						java.util.Currency.getInstance(Locale.CANADA),
						new BigDecimal(totalValueCoins - popCosts[popId]));
				// Return change
				coinsAvailable.returnCoins(change);

				// Store the amount needed.
				coinsAvailable.storeCoins(new Currency(java.util.Currency
						.getInstance(Locale.CANADA), new BigDecimal(
						popCosts[popId])));

				// Return change.
				String changeString = getValueString(totalValueCoins
						- popCosts[popId]);
				System.out.println("Returned Change is : " + changeString);

				// Display vended pop.
				System.out.println(popNames[popId] + " was vended");
			} else {
				// If there is not enough change, return coins and turn on light
				toggleExactChangeLight(true);
				returnReceptacleCoins();

				// Print out to console.
				String coinString = getValueString(totalValueCoins);
				System.out.println("Returned coins: " + coinString);
			}

			// Update Display
			displayUpdatedCoinValue();
		} else {
			// Display that there isn't enough coins and print to console.
			displayPopPrice(popCosts[popId]);
		}
	}

	/**
	 * Toggle the order light.
	 * 
	 * @param turnOn
	 */
	private void toggleOutOfOrderLight(boolean turnOn) {
		if (turnOn) {
			simulator.getOutOfOrderLight().activate();
		} else {
			simulator.getOutOfOrderLight().deactivate();
		}
	}

	/**
	 * Toggle the exact change light.
	 * 
	 * @param turnOn
	 */
	private void toggleExactChangeLight(boolean turnOn) {
		if (turnOn) {
			simulator.getExactChangeLight().activate();
		} else {
			simulator.getExactChangeLight().deactivate();
		}
	}

	/**
	 * Outputs display to command line.
	 * 
	 * @param message
	 */
	private void printDisplay(String message) {
		// Light statuses
		String exactChangeStatus = simulator.getExactChangeLight().isActive() ? "On"
				: "Off";

		String outOfOrderStatus = simulator.getOutOfOrderLight().isActive() ? "On"
				: "Off";

		String lightStatus = String.format(
				"Exact Change: %s\nOut of Order: %s", exactChangeStatus,
				outOfOrderStatus);

		System.out
				.print("--------------------------------------------------------\n"
						+ message
						+ "\n"
						+ lightStatus
						+ "\n"
						+ "--------------------------------------------------------\n");
	}

	/**
	 * Formats a value (in cents) as a dollar value, e.g. $0.00
	 * 
	 * @param value
	 * @return
	 */
	private String getValueString(int value) {
		float amount = ((float) value) / 100;
		return String.format("$%.2f", amount);
	}

	/**
	 * Set display message with currently added coins
	 */
	private void displayUpdatedCoinValue() {
		String value = getValueString(coinsAvailable.getTotalCoinsAvailable());
		simulator.getDisplay().display(value);
		printDisplay(value);
	}

	/**
	 * Display pop price (when not enough coins in receptacle for pop).
	 * 
	 * @param popCost
	 */
	private void displayPopPrice(int popCost) {
		try {
			simulator.getDisplay().display(getValueString(popCost));
			printDisplay(getValueString(popCost));
			Thread.sleep(4000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		printDisplay(getValueString(coinsAvailable.getTotalCoinsAvailable()));
	}

	/**
	 * Display out of stock message.
	 */
	private void displayOutOfStock() {
		try {
			simulator.getDisplay().display("Selected Pop is Out of Stock");
			printDisplay("Selected Pop is Out of Stock");
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
}
