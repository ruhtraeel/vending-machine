package com.jajao.vendingmachine.ui;

import com.jajao.vendingmachine.NotificationManager;
import com.jajao.vendingmachine.ProductDeliveryManager;
import com.jajao.vendingmachine.ProductManager;
import com.vendingmachinesareus.SelectionButton;
import com.vendingmachinesareus.StandardPopVendingMachine;

/**
 * Handle user interaction for a touch interface vending machine
 */
public class TouchUserInteractionManager extends AbstractUserInteractionManager {
    public TouchUserInteractionManager(StandardPopVendingMachine vendingMachine, ProductManager productManager, ProductDeliveryManager productDeliveryManager, NotificationManager notificationManager) {
        super(vendingMachine, productManager, productDeliveryManager, notificationManager);
    }

    public void pressed(SelectionButton selectionButton) {

    }
}
