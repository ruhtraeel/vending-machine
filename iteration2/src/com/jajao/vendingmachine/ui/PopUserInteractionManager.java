package com.jajao.vendingmachine.ui;

import com.jajao.vendingmachine.NotificationManager;
import com.jajao.vendingmachine.Product;
import com.jajao.vendingmachine.ProductDeliveryManager;
import com.jajao.vendingmachine.ProductManager;
import com.vendingmachinesareus.SelectionButton;
import com.vendingmachinesareus.StandardPopVendingMachine;

/**
 * Handle user interaction for a pop vending machine such as
 * pop selection buttons
 */
public class PopUserInteractionManager extends AbstractUserInteractionManager {

    public PopUserInteractionManager(StandardPopVendingMachine vendingMachine, ProductManager productManager, ProductDeliveryManager productDeliveryManager, NotificationManager notificationManager) {
        super(vendingMachine, productManager, productDeliveryManager, notificationManager);
    }

    @Override
    public void pressed (SelectionButton selectionButton) {
        Product product = productManager.getProductForButton(selectionButton);
        ProductDeliveryManager.ReturnCode ReturnCode = productDeliveryManager.dispense(product.getButtonIndex());
        if (ReturnCode == ProductDeliveryManager.ReturnCode.COIN_RACK_FULL) {
            notificationManager.setDisplayMessage("Out of Order");
        } else if (ReturnCode == ProductDeliveryManager.ReturnCode.INSUFFICIENT) {
            notificationManager.setDisplayMessage(product.getCost());
        } else if (ReturnCode == ProductDeliveryManager.ReturnCode.OUT_OF_POP) {
            notificationManager.setDisplayMessage("Out of Stock");
        }
    }
}

