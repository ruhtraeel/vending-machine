package com.jajao.vendingmachine.ui;

import com.jajao.vendingmachine.*;
import com.vendingmachinesareus.*;

/**
 * The User Interaction Manager will handle general
 * user interaction that is common to all vending machine types
 */
public abstract class AbstractUserInteractionManager implements SelectionButtonListener {
    StandardPopVendingMachine vendingMachine;
    ProductManager productManager;
    ProductDeliveryManager productDeliveryManager;
    NotificationManager notificationManager;

    public AbstractUserInteractionManager(StandardPopVendingMachine vM, ProductManager pM, ProductDeliveryManager pDM, NotificationManager nM) {
        this.vendingMachine = vM;
        this.productManager = pM;
        this.productDeliveryManager = pDM;
        this.notificationManager = nM;
        for (int i = 0; i < vendingMachine.getNumberOfSelectionButtons(); i++) {
            SelectionButton button = vendingMachine.getSelectionButton(i);
            button.register(this);
        }

    }

    public void enabled(AbstractHardware abstractHardware) {

    }

    public void disabled(AbstractHardware abstractHardware) {

    }

    // Doesn't exist yet
    public void returnButton() {

    }

    // Abstract method to handle if a selection button is pressed
    abstract public void pressed(SelectionButton selectionButton);
}
