package com.jajao.vendingmachine.ui;

import com.jajao.vendingmachine.NotificationManager;
import com.jajao.vendingmachine.ProductDeliveryManager;
import com.jajao.vendingmachine.ProductManager;
import com.vendingmachinesareus.SelectionButton;
import com.vendingmachinesareus.StandardPopVendingMachine;

/**
 * Handle user interface for a Candy Vending Machine
 * such as the unique combination selection buttons (ex: A1, C4)
 */
public class CandyUserInteractionManager extends AbstractUserInteractionManager {
    public CandyUserInteractionManager(StandardPopVendingMachine vendingMachine, ProductManager productManager, ProductDeliveryManager productDeliveryManager, NotificationManager notificationManager) {
        super(vendingMachine, productManager, productDeliveryManager, notificationManager);
    }

    public void pressed(SelectionButton selectionButton) {

    }
}
