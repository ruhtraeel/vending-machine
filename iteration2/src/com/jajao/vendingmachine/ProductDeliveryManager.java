package com.jajao.vendingmachine;

import com.vendingmachinesareus.CapacityExceededException;
import com.vendingmachinesareus.DisabledException;
import com.vendingmachinesareus.EmptyException;
import com.vendingmachinesareus.StandardPopVendingMachine;

/**
 * Product Delivery Manger will handle delivery of products
 */
public class ProductDeliveryManager {

    public enum ReturnCode {OUT_OF_POP, INSUFFICIENT, SUCCESS, COIN_RACK_FULL}

    private StandardPopVendingMachine vendingMachine;
    private FundsManager fundsManager;
    private FundsDeliveryManager fundsDeliveryManager;

    public ProductDeliveryManager(StandardPopVendingMachine vm, FundsDeliveryManager fdm, FundsManager fm) {
        this.vendingMachine = vm;
        this.fundsDeliveryManager = fdm;
        this.fundsManager = fm;
    }

    /**
     * Dispense pop at rackIndex
     * @param buttonIndex
     */
    public ReturnCode dispense(int buttonIndex) {
        if (fundsManager.isSufficientFunds(buttonIndex)) {
            try {
                vendingMachine.getPopCanRack(buttonIndex).dispensePop();
                fundsDeliveryManager.moveCoinsToRacks();
            } catch (DisabledException e) {
                System.err.println("Vending machine part disabled");
            } catch (EmptyException e) {
                System.err.println("Vending machine is empty");
                return ReturnCode.OUT_OF_POP;
            } catch (CapacityExceededException e) {
                System.err.println("Vending machine product capacity exceeded");
                return ReturnCode.COIN_RACK_FULL;
            }

            // Only return successful if all else is good.
            return ReturnCode.SUCCESS;
        } else {
            return ReturnCode.INSUFFICIENT;
        }
    }
}
