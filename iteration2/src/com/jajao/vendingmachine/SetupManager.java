package com.jajao.vendingmachine;

import com.jajao.vendingmachine.ui.AbstractUserInteractionManager;
import com.jajao.vendingmachine.ui.PopUserInteractionManager;
import com.vendingmachinesareus.*;

public class SetupManager {

    private StandardPopVendingMachine vendingMachine;

    private ProductManager productManager;
    private FundsManager fundsManager;
    private FundsDeliveryManager fundsDeliveryManager;
    private ProductDeliveryManager productDeliveryManager;
    private NotificationManager notificationManager;
    private AbstractUserInteractionManager interactionManager;

    public SetupManager(StandardPopVendingMachine vm, String[] productNames) {
        //---First, instantiate the almighty vending machine.
        this.vendingMachine = vm;

        //---Next, we instantiate the things we know won't need other classes.

        //Product Info Manager
        productManager = new ProductManager(vendingMachine, productNames);

        //Notifications Manager
        notificationManager = new NotificationManager(vendingMachine);

        //---Next, the things that depend on other things.

        //FundsManager
        fundsManager = new FundsManager(vendingMachine, productManager);

        //Delivery managers
        fundsDeliveryManager = new FundsDeliveryManager(vendingMachine);
        productDeliveryManager = new ProductDeliveryManager(vendingMachine, fundsDeliveryManager, fundsManager);

        //Interaction Manager
        interactionManager = new PopUserInteractionManager(vendingMachine, productManager, productDeliveryManager, notificationManager);

    }

    //Get Methods if you get desperate.
    public StandardPopVendingMachine getVendingMachine() {
        return vendingMachine;
    }

    public ProductManager getProductManager() {
        return productManager;
    }

    public FundsManager getFundsManager() {
        return fundsManager;
    }

    public FundsDeliveryManager getFundsDeliveryManager() {
        return fundsDeliveryManager;
    }

    public ProductDeliveryManager getProductDeliveryManager() {
        return productDeliveryManager;
    }

    public NotificationManager getNotificationManager() {
        return notificationManager;
    }

    public AbstractUserInteractionManager getUserInteractionManager() {
        return interactionManager;
    }


    // Methods to change pop quantity and price: Unsure where these should go. Perhaps into a new "SetupHelper" class?

    //Setup Methods
    public void changeProductName(int index, String newName) {
        //No vending machine method to edit name
        productManager.getProduct(index).setName(newName);
    }

    public void changeProductCost(int index, int newCost) {
        vendingMachine.setPopCost(index, newCost);
        productManager.getProduct(index).setCost(newCost);
    }

    //Change the pop quantity knowing the pop rack is empty.
    public void changeProductQuantityEmpty(int rackIndex, int newQuantity) {
        PopCanRack rack = vendingMachine.getPopCanRack(rackIndex);
        int maxCapacity = rack.getCapacity();

        if (newQuantity > maxCapacity)
            throw new RuntimeException(this + "Wanted quantity: " + newQuantity + " > max capacity: " + maxCapacity);

        for (int i = 0; i < newQuantity; i++) {
            try {
                rack.addPop(new PopCan());
            } catch (CapacityExceededException e) {
                e.printStackTrace();
            } catch (DisabledException e) {
                e.printStackTrace();
            }
        }
    }

    //Change the pop quantity knowing the pop rack is partially full.
    //Because we can't see how much pop is in the pop rack, dispense all of it and fill it again.
    public void changeProductQuantityAfterFilled(int rackIndex, int newQuantity) {
        PopCanRack rack = vendingMachine.getPopCanRack(rackIndex);
        int maxCapacity = rack.getCapacity();

        if (newQuantity > maxCapacity)
            throw new RuntimeException(this + "Wanted new quantity was larger than rack max capacity!");

        for (int i = 0; i < maxCapacity; i++) {
            try {
                rack.dispensePop();
                vendingMachine.getDeliveryChute().removeItems();
            } catch (DisabledException e) {
                e.printStackTrace();
            } catch (CapacityExceededException e) {
                throw new RuntimeException(this + "Rack declared it was exceeding capacity while we were emptying it?!");
            } catch (EmptyException e) {
                break;
            }
        }

        changeProductQuantityEmpty(rackIndex, newQuantity);

    }

}
