package com.jajao.vendingmachine;

/**
 * Single Class to keep track of product info used in ProductInfoManager
 */
public class Product {
    String name = "Unnamed";
    int cost = 0;
    int buttonIndex = -1;

    public Product(String name, int cost, int buttonIndex) {
        this.name = name;
        this.cost = cost;
        this.buttonIndex = buttonIndex;
    }

    public String getName() {
        return name;
    }

    public void setName(String newName) {
        name = newName;
    }

    public int getCost() {
        return cost;
    }

    public void setCost(int newCost) {
        cost = newCost;
    }

    public int getButtonIndex() {
        return buttonIndex;
    }
}
