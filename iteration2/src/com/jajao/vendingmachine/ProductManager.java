package com.jajao.vendingmachine;

import com.vendingmachinesareus.SelectionButton;
import com.vendingmachinesareus.StandardPopVendingMachine;

import java.util.ArrayList;

/**
 * Product Info Manager will define all instances of products
 * in the vending machine, and their attributes
 */
public class ProductManager {
    ArrayList<Product> productList = new ArrayList<Product>();

    StandardPopVendingMachine vendingMachine;

    //Constructors
    public ProductManager() {
    }

    public ProductManager(Product[] initialProducts) {
        for (int i = 0; i < initialProducts.length; i++)
            productList.add(initialProducts[i]);
    }

    public ProductManager(StandardPopVendingMachine vendingMachine, String[] productNames) {
        this.vendingMachine = vendingMachine;

        int numberOfSelectionButtons = vendingMachine.getNumberOfSelectionButtons();

        //Extract the popnames from the vending machine.
        //Note: We can't, so we define our own.

        //Extract the default popcosts from the vending machine
        int[] productCosts = new int[numberOfSelectionButtons];
        for (int i = 0; i < productCosts.length; i++)
            productCosts[i] = vendingMachine.getPopCost(i);

        //For button indexes, Create an array like [0,1,2,3...]
        int[] buttonIndexes = new int[numberOfSelectionButtons];
        for (int i = 0; i < buttonIndexes.length; i++)
            buttonIndexes[i] = i;


        //If the lists are wrong
        if (productNames.length != numberOfSelectionButtons)
            throw new IllegalArgumentException(this + " length of name array and number of buttons don't match.");

        for (int i = 0; i < productNames.length; i++)
            productList.add(new Product(productNames[i], productCosts[i], buttonIndexes[i]));
    }

    //Visible Methods

    public Product getProduct(int index) {
        return productList.get(index);
    }

    public Product getProduct(String name) {
        for (int i = 0; i < productList.size(); i++) {
            Product currProduct = productList.get(i);

            if (currProduct.name.equals(name))
                return currProduct;
        }
        return null;
    }

    /**
     * Returns the ProductInfo class that contains the passed button.
     * Important Note: This is not taken from the hardware simulator. The internal button indexes may differ!
     *
     * @param button The button in the hardware simulator.
     * @return the corresponding ProductInfo. Null if it doesn't exist.
     */
    public Product getProductForButton(SelectionButton button) {
        int numberOfSelectionButtons = vendingMachine.getNumberOfSelectionButtons();

        for (int i = 0; i < numberOfSelectionButtons; i++) {
            if (vendingMachine.getSelectionButton(i) == button) {
                return getProductForButton(i);
            }
        }

        return null;
    }


    public Product getProductForButton(int index) {
        for (int i = 0; i < productList.size(); i++) {
            Product currProduct = productList.get(i);

            if (currProduct.buttonIndex == index)
                return currProduct;
        }
        return null;
    }


}
