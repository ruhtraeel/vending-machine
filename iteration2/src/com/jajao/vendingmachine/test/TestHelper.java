package com.jajao.vendingmachine.test;

import com.vendingmachinesareus.CapacityExceededException;
import com.vendingmachinesareus.DisabledException;
import com.vendingmachinesareus.EmptyException;
import com.vendingmachinesareus.StandardPopVendingMachine;

/**
 * Helps reset the vending machine singleton for tests.
 */
public class TestHelper {

    /**
     * Resets the vending machine by emptying out the coin receptacle, and all coin/pop racks.
     */
    public static void resetVendingMachine(StandardPopVendingMachine vendingMachine) {
        // Releases all coins in the coin receptacle and remove from delivery chute.
        try {
            vendingMachine.getCoinReceptacle().returnCoins();
        } catch (CapacityExceededException e) {
            e.printStackTrace();
        } catch (DisabledException e) {
            e.printStackTrace();
        }
        vendingMachine.getDeliveryChute().removeItems();

        // Releases all coins in the coin racks and removes them from the delivery chute.
        for (int i = 0; i < vendingMachine.getNumberOfCoinRacks(); i++) {
            while (true) {
                try {
                    vendingMachine.getCoinRack(i).releaseCoin();
                    vendingMachine.getDeliveryChute().removeItems();
                } catch (CapacityExceededException e) {
                    break;
                } catch (EmptyException e) {
                    break;
                } catch (DisabledException e) {
                    break;
                }
            }
        }

        // Releases all pop cans from the vending machines.
        for (int i = 0; i < vendingMachine.getNumberOfPopCanRacks(); i++) {
            while (true) {
                try {
                    vendingMachine.getPopCanRack(i).dispensePop();
                    vendingMachine.getDeliveryChute().removeItems();
                } catch (DisabledException e) {
                    break;
                } catch (EmptyException e) {
                    break;
                } catch (CapacityExceededException e) {
                    break;
                }
            }
        }
    }
}
