package com.jajao.vendingmachine.test;

import com.jajao.vendingmachine.SetupManager;
import com.vendingmachinesareus.Coin;
import com.vendingmachinesareus.PopCan;
import com.vendingmachinesareus.StandardPopVendingMachine;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class TestFundsManager
{

    SetupManager setupManager;
    StandardPopVendingMachine vendingMachine;
    
    /**
     * @throws Exception
     */
    @Before
    public void before() throws Exception {
        setupManager = new SetupManager(StandardPopVendingMachine.getInstance(), new String[]{"Pepsi", "Coke", "Nestea", "OdaBuddy", "AndyCola", "Jaderade"});
        vendingMachine = setupManager.getVendingMachine();
    }

    /**
     * @throws Exception
     */
    @After
    public void after() throws Exception {
        TestHelper.resetVendingMachine(vendingMachine);
        vendingMachine = null;
    }

    /**
     * Method : will test if sufficient funds is working the way it should
     * Test for true boolean value
     * @throws Exception handled by hardware and software
     */
    @Test
    public void isSufficientFundsTestTrue() throws Exception {
        vendingMachine.getPopCanRack(0).addPop(new PopCan());
        vendingMachine.getCoinSlot().addCoin(new Coin(200));
        assert (setupManager.getFundsManager().isSufficientFunds(0));
    }

    /**
     * Method : will test if sufficient funds is working the way it should
     * Test for false boolean value
     * @throws Exception handled by hardware and software
     */
    @Test
    public void isSufficientFundsTestFalse() throws Exception {
        vendingMachine.getPopCanRack(0).addPop(new PopCan());
        assert (!setupManager.getFundsManager().isSufficientFunds(0));
    }
}
