package com.jajao.vendingmachine.test;

import com.jajao.vendingmachine.*;
import com.vendingmachinesareus.*;
import org.junit.*;

/**
* SetupManager Tester.
* Checks that the methods return values akin to the method descriptions.
*
* Note: Get methods aren't tested. Way too simple.
*/
public class TestSetupManager02
{

    SetupManager setupManager;
    StandardPopVendingMachine vendingMachine;

    @Before
    public void before() throws Exception {
        vendingMachine = StandardPopVendingMachine.getInstance();
        setupManager = new SetupManager(vendingMachine, new String[] {"foo","bar","lol","pizza","papaJohns","turkey"});
    }

    @After
    public void after() throws Exception {
        TestHelper.resetVendingMachine(vendingMachine);
        vendingMachine = null;
        setupManager = null;
    }


    /**
     * Method: changeProductName(int index, String newName)
     */
    @Test
    public void testChangeProductName() throws Exception {
        int productIndex = 0;
        String name = "foobario";

        setupManager.changeProductName(productIndex,name);

        ProductManager productManager = setupManager.getProductManager();

        assert (productManager.getProduct(productIndex).getName().equals(name));
    }

    /**
     * Method: changeProductCost(int index, int newCost)
     */
    @Test
    public void testChangeProductCost() throws Exception {
        int productIndex = 0;
        int newCost = 2000;

        setupManager.changeProductCost(productIndex,newCost);

        ProductManager productManager = setupManager.getProductManager();
        int productManagerCost = productManager.getProduct(productIndex).getCost();

        int hardwareCost = vendingMachine.getPopCost(productIndex);

        assert (productManagerCost == hardwareCost);
        assert (hardwareCost == newCost);
    }

    /**
     * Method: changeProductQuantityEmpty(int rackIndex, int newQuantity)
     * Make sure we fill the rack with the right amount of pop.
     */
    @Test
    public void testChangeProductQuantityEmpty() throws Exception
    {
        int popRackIndex = 0;
        int newQuantity = 10;
        newQuantity = Math.min(newQuantity,vendingMachine.getPopCanRack(popRackIndex).getCapacity());

        //Load our pop.
        setupManager.changeProductQuantityEmpty(popRackIndex, newQuantity);

        //Dispence it all
        for (int i = 0; i < newQuantity; i++)
        {
            vendingMachine.getPopCanRack(popRackIndex).dispensePop();
            assert (vendingMachine.getDeliveryChute().removeItems().length == 1);
        }

        //Make sure we get the empty exception
        boolean emptyExceptionThrown = false;
        try {
            vendingMachine.getPopCanRack(popRackIndex).dispensePop();
        }
        catch (EmptyException e)
        {
            emptyExceptionThrown = true;
        }

        assert (emptyExceptionThrown);
    }

    /**
     * Method: changeProductQuantityAfterFilled(int rackIndex, int newQuantity)
     * Make sure the QuantityAfterFilled fills the rack with the correct number of pop
     */
    @Test
    public void testChangeProductQuantityAfterFilled() throws Exception
    {
        int popRackIndex = 0;
        int initalQuantity = 10;
        int newQuantity = 15;

        setupManager.changeProductQuantityEmpty(popRackIndex,initalQuantity);

        setupManager.changeProductQuantityAfterFilled(popRackIndex,newQuantity);


        //Dispence it all
        for (int i = 0; i < newQuantity; i++)
        {
            vendingMachine.getPopCanRack(popRackIndex).dispensePop();
            assert (vendingMachine.getDeliveryChute().removeItems().length == 1);
        }

        //Make sure we get the empty exception
        boolean emptyExceptionThrown = false;
        try {
            vendingMachine.getPopCanRack(popRackIndex).dispensePop();
        }
        catch (EmptyException e)
        {
            emptyExceptionThrown = true;
        }

        assert (emptyExceptionThrown);

    }


}
