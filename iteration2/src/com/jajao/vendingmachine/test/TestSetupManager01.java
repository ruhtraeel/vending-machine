package com.jajao.vendingmachine.test;

import com.jajao.vendingmachine.SetupManager;
import com.vendingmachinesareus.Coin;
import com.vendingmachinesareus.DisabledException;
import com.vendingmachinesareus.StandardPopVendingMachine;
import org.junit.After;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import static org.junit.Assert.assertTrue;

/**
 * Tests the SetupManager class methods that change the vending machine.
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class TestSetupManager01
{
    SetupManager setupManager;
    StandardPopVendingMachine vendingMachine;

    @Before
    public void setUp() {
        vendingMachine = StandardPopVendingMachine.getInstance();
        setupManager = new SetupManager(vendingMachine,
                new String[]{"Pop1", "Pop2", "Pop3", "Pop4", "Pop5", "Pop6"});
    }

    @After
    public void tearDown() {
        TestHelper.resetVendingMachine(vendingMachine);
        vendingMachine = null;
        setupManager = null;
    }

    /**
     * Tests the changeProductQuantityEmpty to change the quantity of a rack.
     */
    @Test
    public void test1ChangeProductQuantityEmpty() {
        // Try to dispense a pop from an empty rack.
        try {
            vendingMachine.getCoinSlot().addCoin(new Coin(100));
        } catch (DisabledException e) {
            e.printStackTrace();
        }

        setupManager.changeProductQuantityEmpty(0, 5);
    }
}
