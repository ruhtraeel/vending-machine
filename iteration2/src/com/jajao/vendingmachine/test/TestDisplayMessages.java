package com.jajao.vendingmachine.test;

import com.jajao.vendingmachine.NotificationManager;
import com.jajao.vendingmachine.SetupManager;
import com.vendingmachinesareus.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static junit.framework.TestCase.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * Tests that the vending machine displays the correct message under all situations.
 */
public class TestDisplayMessages {
    SetupManager setupManager;
    StandardPopVendingMachine vendingMachine;

    @Before
    public void setUp() {
        vendingMachine = StandardPopVendingMachine.getInstance();
        setupManager = new SetupManager(vendingMachine, new String[]{"Pop1", "Pop2", "Pop3", "Pop4", "Pop5", "Pop6"});
    }

    @After
    public void tearDown() {
        TestHelper.resetVendingMachine(vendingMachine);
        vendingMachine = null;
        setupManager = null;
    }

    @Test
    public void testDisplayOutOfStock() {
        String expectedMessage = "Out of Stock";

        // Empty out vending machine's stock of product 0.
        setupManager.changeProductQuantityEmpty(0, 0);

        // Insert enough funds for pop.
        try {
            vendingMachine.getCoinSlot().addCoin(new Coin(100));
        } catch (DisabledException e) {
            e.printStackTrace();
        }

        // Ensure that there is enough funds for the pop.
        assertTrue(setupManager.getFundsManager().isSufficientFunds(0));

        // Press the button corresponding to the desired product 0.
        vendingMachine.getSelectionButton(0).press();

        // Check that display says "Out of Stock".
        String displayMessage = vendingMachine.getDisplay().read();
        assertTrue(displayMessage.equals(expectedMessage));
    }

    /**
     * Test that the display shows "Out of Order".
     */
    @Test
    public void testDisplayOutOfOrder() {
        String expectedMessage = "Out of Order";

        // Fill pop rack 0.
        for (int i = 0; i < vendingMachine.getPopCanRack(0).getCapacity(); i++) {
            try {
                vendingMachine.getPopCanRack(0).addPop(new PopCan());
            } catch (CapacityExceededException e) {
                System.err.println("Product rack capacity exceeded");
            } catch (DisabledException e) {
                e.printStackTrace();
            }
        }

        // TODO: Invalid. need to know denominations for coin racks.

        // Check that the display says Out of Order.
        String displayMessage = vendingMachine.getDisplay().read();
        assertTrue(displayMessage.equals(expectedMessage));
    }

    /**
     * Test that the cost of the selected pop is displayed when there are insufficient funds.
     */
    @Test
    public void testDisplayInsufficientFunds() {
        String expectedMessage = NotificationManager.getValueString(vendingMachine.getPopCost(0));

        // Fill product rack.
        setupManager.changeProductQuantityEmpty(0, 5);

        // Do not insert coin.
        assertFalse(setupManager.getFundsManager().isSufficientFunds(0));

        // Press the button corresponding to the desired product 0.
        vendingMachine.getSelectionButton(0).press();

        // Check that display says the price of the desired product.
        String displayMessage = vendingMachine.getDisplay().read();
        assertTrue(displayMessage.equals(expectedMessage));
    }
}
