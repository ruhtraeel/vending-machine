package com.jajao.vendingmachine.test;

import com.jajao.vendingmachine.*;
import com.vendingmachinesareus.*;
import org.junit.*;

/**
 * ProductManager Tester.
 * Make sure methods return right values for their description.
*/

public class TestProductManager
{
    ProductManager productManager;
    Product[] defaultInput = new Product[] {new Product("Pepsi",100,0),new Product("Coke",200,1),new Product("JanDaMan",300,2)};

    @Before
    public void before() throws Exception {
        productManager = new ProductManager(defaultInput);
    }

    @After
    public void after() throws Exception {
        productManager = null;
    }

    /**
     * Method: getProduct(int index)
     */
    @Test
    public void testGetProductIndex() throws Exception {

        for (int i = 0; i < defaultInput.length; i++)
        {
            Product managerProduct = productManager.getProduct(i);
            Product inputProduct = defaultInput[i];

            assert (managerProduct.getName().equals(inputProduct.getName()));
            assert (managerProduct.getCost() == inputProduct.getCost());
            assert (managerProduct.getButtonIndex() == inputProduct.getButtonIndex());
        }
    }

    /**
     * Method: getProduct(String name)
     */
    @Test
    public void testGetProductName() throws Exception {
        for (int i = 0; i < defaultInput.length; i++)
        {
            Product inputProduct = defaultInput[i];
            assert (productManager.getProduct(inputProduct.getName()) == inputProduct);
        }
    }

    /**
     * Method: getProductForButton(SelectionButton button)
     */
    @Test
    public void testGetProductForButtonButton() throws Exception {
        StandardPopVendingMachine vendingMachine = StandardPopVendingMachine.getInstance();
        SetupManager setupManager = new SetupManager(vendingMachine,new String[] {"foo","bar","lol","John","Bar","foo"});
        productManager = setupManager.getProductManager();

        for (int i = 0; i < vendingMachine.getNumberOfSelectionButtons(); i++)
        {
            SelectionButton currButton = vendingMachine.getSelectionButton(i);
            Product currProduct = productManager.getProductForButton(currButton);

            assert (currProduct.getButtonIndex() == i);
        }

        TestHelper.resetVendingMachine(vendingMachine);
        vendingMachine = null;

    }

    /**
     * Method: getProductForButton(int index)
     */
    @Test
    public void testGetProductForButtonIndex() throws Exception {
        for (int i = 0; i < defaultInput.length; i++)
        {
            Product productFromButtonIndex =  productManager.getProductForButton(i);
            assert (productFromButtonIndex == defaultInput[i]);
        }
    }


}
