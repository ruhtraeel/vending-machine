package com.jajao.vendingmachine.test;

import com.jajao.vendingmachine.SetupManager;
import com.vendingmachinesareus.Coin;
import com.vendingmachinesareus.DisabledException;
import com.vendingmachinesareus.StandardPopVendingMachine;
import org.junit.After;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * Test PopUserInteractionManager to ensure that the selection buttons react properly.
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class TestSelectProduct {
    SetupManager setupManager;
    StandardPopVendingMachine vendingMachine;

    @Before
    public void setUp() {
        vendingMachine = StandardPopVendingMachine.getInstance();
        setupManager = new SetupManager(vendingMachine, new String[]{"Pepsi", "Coke", "Nestea", "OdaBuddy", "AndyCola", "Jaderade"});

        // Add Pop cans to rack 1.
        setupManager.changeProductQuantityEmpty(0, 5);
    }

    @After
    public void tearDown() {
        setupManager = null;

        TestHelper.resetVendingMachine(vendingMachine);
        vendingMachine = null;
    }

    /**
     * Tests an unsuccessful vend: does not dispense upon button press because of insufficient funds.
     */
    @Test
    public void test1SelectProductNotEnoughFunds() {
        // Do not insert coin.
        assertFalse(setupManager.getFundsManager().isSufficientFunds(0));

        // Check that there is nothing in the delivery chute before button press.
        Object products[] = vendingMachine.getDeliveryChute().removeItems();
        assertTrue(products.length == 0);

        // Press the button corresponding to the desired product 0.
        vendingMachine.getSelectionButton(0).press();

        // Check that there is still nothing in the delivery chute after button press.
        products = vendingMachine.getDeliveryChute().removeItems();
        assertTrue(products.length == 0);
    }

    /**
     * Tests a successful vend, that given enough funds for a product, dispenses a pop into the chute upon button press.
     */
    @Test
    public void test2SelectProductEnoughFunds() {
        // Insert valid coin.
        try {
            vendingMachine.getCoinSlot().addCoin(new Coin(100));
        } catch (DisabledException e) {
            e.printStackTrace();
        }

        // Check that we have enough funds for this pop.
        assertTrue(setupManager.getFundsManager().isSufficientFunds(0));

        // Check that there is nothing in the delivery chute before button press.
        Object products[] = vendingMachine.getDeliveryChute().removeItems();
        assertTrue(products.length == 0);

        // Press the button corresponding to the desired product 0.
        vendingMachine.getSelectionButton(0).press();

        // Check that there is a product now after the button press.
        products = vendingMachine.getDeliveryChute().removeItems();
        assertTrue(products.length == 1);
    }
}
