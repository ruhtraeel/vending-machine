package com.jajao.vendingmachine.test;

import com.jajao.vendingmachine.*;
import com.vendingmachinesareus.*;
import org.junit.*;

/**
 * NotificationManager Tester.
 * Tests if common functionality in the Notification manager does what it's supposed to.
 */
public class TestNotificationManager
{

    StandardPopVendingMachine vendingMachine;
    NotificationManager notificationManager;

    @Before
    public void before() throws Exception {
        vendingMachine = StandardPopVendingMachine.getInstance();
        notificationManager = new NotificationManager(vendingMachine);
    }

    @After
    public void after() throws Exception {
        notificationManager = null;
        TestHelper.resetVendingMachine(vendingMachine);
        vendingMachine = null;
    }

    /**
     * Method: setDisplayMessage(String message)
     * <p/>
     * Test That we can display the correct messages in the vending machine
     */
    @Test
    public void testSetDisplayMessage() throws Exception {

        String messageToDisplay = "foobar123?! \"ReversedQuotes\"";
        notificationManager.setDisplayMessage(messageToDisplay);
        String result = vendingMachine.getDisplay().read();

        assert (result.equals(messageToDisplay));

        messageToDisplay = "aDifferentMessage";
        notificationManager.setDisplayMessage(messageToDisplay);
        result = vendingMachine.getDisplay().read();

        assert (result.equals(messageToDisplay));
    }

    /**
     * Method: setDisplayMessageListener(String message)
     * <p/>
     * Test That we can display the correct messages in the vending machine. Also that the listener sends out the right string.
     */
    @Test
    public void testSetDisplayMessageListener() throws Exception {

        class DisplayListenerTester implements DisplayListener
        {
            public Display display;
            public String s;
            public String s1;

            @Override
            public void messageChange(Display display, String s, String s1) {
                this.display = display;
                this.s = s;
                this.s1 = s1;
            }

            @Override
            public void enabled(AbstractHardware<AbstractHardwareListener> abstractHardware) {}
            @Override
            public void disabled(AbstractHardware<AbstractHardwareListener> abstractHardware) {}
        }

        DisplayListenerTester tester = new DisplayListenerTester();
        vendingMachine.getDisplay().register(tester);

        //Start test
        String messageToDisplay = "321barfoo?! \"HereWeGoAgain\"";
        notificationManager.setDisplayMessage(messageToDisplay);

        String result = tester.s;

        assert (result.equals(messageToDisplay));
    }

    /**
     * Method: setIndicatorLightStatus(IndicatorLight light, boolean on)
     * Test that our indicator light acts as we expect
     */
    @Test
    public void testSetIndicatorLightStatus() throws Exception {
        IndicatorLight light = vendingMachine.getOutOfOrderLight();
        notificationManager.setIndicatorLightStatus(light, true);

        assert (light.isActive());

        notificationManager.setIndicatorLightStatus(light, false);

        assert (light.isActive() == false);
    }

} 
