package com.jajao.vendingmachine;

import com.vendingmachinesareus.*;

import java.util.ArrayList;
import java.util.List;

/**
 * FundsManager manages all funds available for the machine and the user
 * These funds are defined as : Coins, Credit Cards and Pre Paid Cards
 * <p/>
 * The manager will handle all fund logic
 */
public class FundsManager implements CoinReceptacleListener {

    private StandardPopVendingMachine vendingMachine;
    private ProductManager productManager;
    private List<Coin> currentReceptacleCoins = new ArrayList<Coin>();

    public FundsManager(StandardPopVendingMachine vm, ProductManager pim) {
        this.vendingMachine = vm;
        this.productManager = pim;
        vendingMachine.getCoinReceptacle().register(this);
    }

    /**
     * Method called to check if sufficient funds in receptacle (coins only)
     * or sufficient funds in card (pre paid)
     * will compare to price of product selected via rack index
     *
     * @param buttonIndex
     * @return True if sufficient funds, False if not
     */
    public boolean isSufficientFunds(int buttonIndex) {
        int receptacleValue = getValueOfReceptacle();
        int cost = productManager.getProduct(buttonIndex).getCost();

        return receptacleValue >= cost;
    }

    /**
     * Method used to get the current value of the receptacle
     *
     * @return int value of receptacle
     */
    private int getValueOfReceptacle() {
        int temp = 0;
        for (int i = 0; i < currentReceptacleCoins.size(); i++) {
            Coin tempCoin = currentReceptacleCoins.get(i);
            temp += tempCoin.getValue();
        }
        return temp;
    }

    public void coinAdded(CoinReceptacle coinReceptacle, Coin coin) {
        currentReceptacleCoins.add(coin);
    }

    public void coinsRemoved(CoinReceptacle coinReceptacle) {
        currentReceptacleCoins.clear();
    }

    public void coinsFull(CoinReceptacle coinReceptacle) {

    }

    public void enabled(CoinReceptacle coinReceptacle) {

    }

    public void disabled(CoinReceptacle coinReceptacle) {

    }

    public void disabled(AbstractHardware abstractHardware) {

    }

    public void enabled(AbstractHardware abstractHardware) {

    }
}
