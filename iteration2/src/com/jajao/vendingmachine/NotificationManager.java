package com.jajao.vendingmachine;

import com.vendingmachinesareus.IndicatorLight;
import com.vendingmachinesareus.StandardPopVendingMachine;

/**
 * Notification Manger will listen to triggers in the system
 * to notify the user using lights and a display unit
 */
public class NotificationManager {

    private StandardPopVendingMachine vendingMachine;

    public NotificationManager(StandardPopVendingMachine vm) {
        this.vendingMachine = vm;
    }

    /**
     * Sets the display message to a funds value in dollars.
     *
     * @param value indicating funds in cents.
     */
    public void setDisplayMessage(int value) {
        vendingMachine.getDisplay().display(getValueString(value));
    }

    /**
     * Sets the message of the display.
     *
     * @param message the message to be displayed on the display.
     */
    public void setDisplayMessage(String message) {
        vendingMachine.getDisplay().display(message);
    }

    /**
     * Sets the light status of a light indicator.
     *
     * @param light the light indicator to set
     * @param on    boolean indicating the status to set to: true for on, false for off.
     */
    public void setIndicatorLightStatus(IndicatorLight light, boolean on) {
        if (on)
            light.activate();
        else
            light.deactivate();
    }

    /**
     * Returns a formatted value string: $x.xx
     *
     * @param value funds in cents
     * @return a string representation of funds
     */
    public static String getValueString(int value) {
        return String.format("$%.2f", ((double) value) / 100);
    }
}
