package com.jajao.vendingmachine;

import com.vendingmachinesareus.CapacityExceededException;
import com.vendingmachinesareus.DisabledException;
import com.vendingmachinesareus.StandardPopVendingMachine;

/**
 * Funds Delivery manager will handle the deliver of funds within the machine
 */
public class FundsDeliveryManager {

    private StandardPopVendingMachine vendingMachine;

    public FundsDeliveryManager(StandardPopVendingMachine vm) {

        this.vendingMachine = vm;
    }

    /**
     * Method will move coins from receptacle to racks once a successful
     * purchase is made, handle errors as needed
     */
    public void moveCoinsToRacks() {
        try {
            vendingMachine.getCoinReceptacle().storeCoins();
        } catch (CapacityExceededException e) {
            e.printStackTrace();
        } catch (DisabledException e) {
            e.printStackTrace();
        }
    }

    /**
     * Move the coins from a location to the delivery chute
     * for the user to take, different types indicated by index value
     *
     * @param type
     */
    public void moveCoinsToChute(int type) {
        switch (type) {
            // Case for returning all coins from receptacle to chute
            case 0:
                try {
                    vendingMachine.getCoinReceptacle().returnCoins();
                } catch (CapacityExceededException e) {
                    e.printStackTrace();
                } catch (DisabledException e) {
                    e.printStackTrace();
                }
                break;
            // Case for moving coins from racks to chute (change)
            case 1:
                break;
        }

    }

    /**
     * METHOD TAKEN FROM ITERATION 1
     * <p/>
     * Attempts to return the requested amount, one coin at a time.
     *
     * @param amount The amount in cents to be dispensed
     * @return Any residual amount that was not dispensable.
     */
    private int dispense(int amount) {
//        int racks = vendingMachine.getNumberOfCoinRacks();
//        for (int i = racks - 1; i >= 0; i--) {
//            CoinRack rack = vendingMachine.getCoinRack(i);
//
//            int value = rackDenominations[i];
//
//            coinLoop: while (amount >= value) {
//                try {
//                    rack.releaseCoin();
//                } catch (CapacityExceededException e) {
//                    e.printStackTrace();
//                    return amount;
//                } catch (EmptyException e) {
//                    e.printStackTrace();
//                    break coinLoop;
//                } catch (DisabledException e) {
//                    e.printStackTrace();
//                    return amount;
//                }
//                amount -= value;
//            }
//        }
//        return amount;
        return 1;
    }

    /**
     * Get change necessary after a purchase is made
     * This will grab coins from coin rack and move them
     * them through the chute
     *
     * @param amount
     */
    public void getChange(int amount) {
        dispense(amount);
    }
}
