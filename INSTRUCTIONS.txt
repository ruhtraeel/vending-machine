SENG 403: Iteration 1
Andrei Savu, Arthur Lee, Jade White, Jan Clarin, Olabode Adegbayike
src/main/TestDriver.java provides an example of how to run the Driver class.
Run the main function in the class in order to do a simulation of the vending machine.

In order to run the tests, use JUnit to run them. The test classes are located in org.lsmr.vendingmachine.simulator/src/test 

It is recommended to import the project into eclipse.
